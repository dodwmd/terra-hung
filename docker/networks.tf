resource "docker_network" "puppet" {
    name = "puppet"
    driver = "overlay"
}

resource "docker_network" "logging" {
    name = "logging"
    driver = "overlay"
}

resource "docker_network" "tooling" {
    name = "tooling"
    driver = "overlay"
}

resource "docker_network" "proxy" {
    name = "proxy"
    driver = "overlay"
}
