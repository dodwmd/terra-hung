# Configure the Docker provider
provider "docker" {
#  cert_path = "./certs/"
# Workaround for 0.8 support
  cert_material = "${file("../aws/files/certs/client.pem")}"
  key_material = "${file("../aws/files/certs/client-key.pem")}"
  ca_material = "${file("../aws/files/certs/ca.pem")}"
  host = "tcp://docker.hung.ts0.org:2376/"
}
