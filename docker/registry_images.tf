########################################################
# Docker Containers
########################################################

data "docker_registry_image" "registry" {
  name = "registry:2"
}

########################################################
# Proxy Containers
########################################################

data "docker_registry_image" "nginx" {
  name = "dodwmd/nginx:latest"
}

data "docker_registry_image" "nginx_gen" {
  name = "jwilder/docker-gen:latest"
}

data "docker_registry_image" "nginx_letsencrypt" {
  name = "jrcs/letsencrypt-nginx-proxy-companion:latest"
}

########################################################
# Tooling Containers
########################################################

data "docker_registry_image" "bamboo" {
  name = "dodwmd/bamboo:latest"
}

data "docker_registry_image" "artifactory" {
  name = "dodwmd/artifactory:latest"
}

data "docker_registry_image" "splunk" {
  name = "dodwmd/splunk:latest"
}

########################################################
# MuleSoft Containers
########################################################

data "docker_registry_image" "appdynamics" {
  name = "dodwmd/appdynamics:latest"
}

data "docker_registry_image" "mmc" {
  name = "dodwmd/mmc:latest"
}

########################################################
# Puppet Containers
########################################################

data "docker_registry_image" "puppet" {
  name = "puppet/puppetserver:latest"
}

data "docker_registry_image" "puppetdb-postgres" {
  name = "puppet/puppetdb-postgres:latest"
}

data "docker_registry_image" "puppetdb" {
  name = "puppet/puppetdb:latest"
}

data "docker_registry_image" "puppetboard" {
  name = "puppet/puppetboard:latest"
}

data "docker_registry_image" "puppetexplorer" {
  name = "puppet/puppetexplorer:latest"
}

########################################################
# Logging Containers
########################################################

data "docker_registry_image" "filebeat" {
  name = "prima/filebeat:1"
}

data "docker_registry_image" "logstash" {
  name = "logstash:5.1.1"
}

data "docker_registry_image" "elasticsearch" {
  name = "elasticsearch:5.1.1"
}

data "docker_registry_image" "curator" {
  name = "bobrik/curator:latest"
}

data "docker_registry_image" "kibana" {
  name = "kibana:5.1.1"
}

########################################################
# Monitoring Containers
########################################################

data "docker_registry_image" "node_exporter" {
  name = "prom/node-exporter:latest"
}

data "docker_registry_image" "cadvisor" {
  name = "google/cadvisor:latest"
}

data "docker_registry_image" "prometheus" {
  name = "prom/prometheus:latest"
}

data "docker_registry_image" "grafana" {
  name = "grafana/grafana:latest"
}

data "docker_registry_image" "alertmanager" {
  name = "prom/alertmanager:latest"
}

