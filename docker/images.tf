########################################################
# Docker related Containers
########################################################

resource "docker_image" "registry" {
  name = "${data.docker_registry_image.registry.name}"
  pull_trigger = "${data.docker_registry_image.registry.sha256_digest}"
}

########################################################
# Proxy Containers
########################################################

resource "docker_image" "nginx" {
  name = "${data.docker_registry_image.nginx.name}"
  pull_trigger = "${data.docker_registry_image.nginx.sha256_digest}"
}

resource "docker_image" "nginx_gen" {
  name = "${data.docker_registry_image.nginx_gen.name}"
  pull_trigger = "${data.docker_registry_image.nginx_gen.sha256_digest}"
}

resource "docker_image" "nginx_letsencrypt" {
  name = "${data.docker_registry_image.nginx_letsencrypt.name}"
  pull_trigger = "${data.docker_registry_image.nginx_letsencrypt.sha256_digest}"
}

########################################################
# Tooling Containers
########################################################

resource "docker_image" "bamboo" {
  name = "${data.docker_registry_image.bamboo.name}"
  pull_trigger = "${data.docker_registry_image.bamboo.sha256_digest}"
}

resource "docker_image" "artifactory" {
  name = "${data.docker_registry_image.artifactory.name}"
  pull_trigger = "${data.docker_registry_image.artifactory.sha256_digest}"
}

resource "docker_image" "splunk" {
  name = "${data.docker_registry_image.splunk.name}"
  pull_trigger = "${data.docker_registry_image.splunk.sha256_digest}"
}

########################################################
# Puppet Containers
########################################################

resource "docker_image" "puppet" {
  name = "${data.docker_registry_image.puppet.name}"
  pull_trigger = "${data.docker_registry_image.puppet.sha256_digest}"
}

resource "docker_image" "puppetdb-postgres" {
  name = "${data.docker_registry_image.puppetdb-postgres.name}"
  pull_trigger = "${data.docker_registry_image.puppetdb-postgres.sha256_digest}"
}

resource "docker_image" "puppetdb" {
  name = "${data.docker_registry_image.puppetdb.name}"
  pull_trigger = "${data.docker_registry_image.puppetdb.sha256_digest}"
}

resource "docker_image" "puppetboard" {
  name = "${data.docker_registry_image.puppetboard.name}"
  pull_trigger = "${data.docker_registry_image.puppetboard.sha256_digest}"
}

resource "docker_image" "puppetexplorer" {
  name = "${data.docker_registry_image.puppetexplorer.name}"
  pull_trigger = "${data.docker_registry_image.puppetexplorer.sha256_digest}"
}

########################################################
# MuleSoft Containers
########################################################

resource "docker_image" "appdynamics" {
  name = "${data.docker_registry_image.appdynamics.name}"
  pull_trigger = "${data.docker_registry_image.appdynamics.sha256_digest}"
}

resource "docker_image" "mmc" {
  name = "${data.docker_registry_image.mmc.name}"
  pull_trigger = "${data.docker_registry_image.mmc.sha256_digest}"
}

########################################################
# Logging Containers
########################################################

resource "docker_image" "filebeat" {
  name = "${data.docker_registry_image.filebeat.name}"
  pull_trigger = "${data.docker_registry_image.filebeat.sha256_digest}"
}

resource "docker_image" "logstash" {
  name = "${data.docker_registry_image.logstash.name}"
  pull_trigger = "${data.docker_registry_image.logstash.sha256_digest}"
}

resource "docker_image" "elasticsearch" {
  name = "${data.docker_registry_image.elasticsearch.name}"
  pull_trigger = "${data.docker_registry_image.elasticsearch.sha256_digest}"
}

resource "docker_image" "curator" {
  name = "${data.docker_registry_image.curator.name}"
  pull_trigger = "${data.docker_registry_image.curator.sha256_digest}"
}

resource "docker_image" "kibana" {
  name = "${data.docker_registry_image.kibana.name}"
  pull_trigger = "${data.docker_registry_image.kibana.sha256_digest}"
}

########################################################
# Monitoring Containers
########################################################

resource "docker_image" "node_exporter" {
  name = "${data.docker_registry_image.node_exporter.name}"
  pull_trigger = "${data.docker_registry_image.node_exporter.sha256_digest}"
}

resource "docker_image" "cadvisor" {
  name = "${data.docker_registry_image.cadvisor.name}"
  pull_trigger = "${data.docker_registry_image.cadvisor.sha256_digest}"
}

resource "docker_image" "prometheus" {
  name = "${data.docker_registry_image.prometheus.name}"
  pull_trigger = "${data.docker_registry_image.prometheus.sha256_digest}"
}

resource "docker_image" "grafana" {
  name = "${data.docker_registry_image.grafana.name}"
  pull_trigger = "${data.docker_registry_image.grafana.sha256_digest}"
}

resource "docker_image" "alertmanager" {
  name = "${data.docker_registry_image.alertmanager.name}"
  pull_trigger = "${data.docker_registry_image.alertmanager.sha256_digest}"
}

