########################################################
# Docker related Containers
########################################################
# create registry container
resource "docker_container" "registry" {
  image = "${docker_image.registry.latest}"
  name = "registry"
  hostname = "registry"
  env = ["REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt", "REGISTRY_HTTP_TLS_KEY=/certs/domain-key.crt"]
  must_run="true"
  ports {
    internal = 5000
    external = 5000
  }
  volumes {
    container_path  = "/certs"
    host_path = "/certs/registry"
    read_only = true
  }

  labels = {
    "container_group" = "docker"
  }
  env = [ "VIRTUAL_HOST=registry.hung.ts0.org", "LETSENCRYPT_HOST=registry.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true" ]
  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

########################################################
# Web Containers
########################################################
# create nginx reverse proxy container
resource "docker_container" "nginx" {
  image = "${docker_image.nginx.latest}"
  name = "nginx"
  hostname = "nginx"
  must_run="true"
  networks = [ "${docker_network.logging.name}", "${docker_network.tooling.name}", "${docker_network.proxy.name}", "${docker_network.puppet.name}" ]

  ports {
    internal = 80
    external = 80
  }

  ports {
    internal = 443
    external = 443
  }

  volumes {
    host_path  = "/storage/nginx/conf.d"
    container_path = "/etc/nginx/conf.d"
    read_only = false
  }

  volumes {
    host_path  = "/storage/nginx/vhost.d"
    container_path = "/etc/nginx/vhost.d"
    read_only = false
  }

  volumes {
    host_path  = "/storage/nginx/html"
    container_path = "/var/www/html"
    read_only = false
  }

  volumes {
    host_path  = "/storage/nginx/certs"
    container_path = "/etc/nginx/certs"
    read_only = false
  }

  volumes {
    host_path  = "/storage/nginx/htpasswd"
    container_path = "/etc/nginx/htpasswd"
    read_only = false
  }

  env = [ "DEFAULT_HOST=www.hung.ts0.org" ]

  depends_on = ["docker_container.bamboo","docker_container.artifactory"]

  labels = {
    "container_group" = "proxy"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

#resource "docker_container" "nginx_gen" {
#  image = "${docker_image.nginx_gen.latest}"
#  name = "nginx_gen"
#  hostname = "nginx_gen"
#  must_run="true"
#  networks = [ "${docker_network.proxy.name}" ]
#  depends_on = ["docker_container.nginx"]
#
#  volumes {
#    host_path  = "/var/run/docker.sock"
#    container_path = "/tmp/docker.sock"
#    read_only = true
#  }
#
#  volumes {
#    host_path  = "/storage/dockergen/nginx.tmpl"
#    container_path = "/etc/docker-gen/templates/nginx.tmpl"
#    read_only = true
#  }
#
#  volumes {
#    from_container  = "nginx"
#  }
#
#  env = [ "DEFAULT_HOST=hung.ts0.org" ]
#
#  command = [ "-notify-sighup", "nginx", "-watch", "/etc/docker-gen/templates/nginx.tmpl", "/etc/nginx/conf.d/default.conf" ]
#
#  labels = {
#    "container_group" = "proxy"
#  }
#
#  log_driver = "gelf"
#  log_opts = {
#    "gelf-address" = "udp://localhost:12201"
#    "labels" = "container_group"
#  }
#
#}
#
#resource "docker_container" "nginx_letsencrypt" {
#  image = "${docker_image.nginx_letsencrypt.latest}"
#  name = "nginx-letsencrypt"
#  hostname = "nginx-letsencrypt"
#  must_run="true"
#  networks = [ "${docker_network.proxy.name}" ]
#  depends_on = ["docker_container.nginx_gen"]
#
#  volumes {
#    host_path  = "/var/run/docker.sock"
#    container_path = "/var/run/docker.sock"
#    read_only = true
#  }
#
#  volumes {
#    from_container  = "nginx"
#  }
#
#  volumes {
#    host_path  = "/storage/nginx/certs"
#    container_path = "/etc/nginx/certs"
#    read_only = false
#  }
#
#  env = [ "NGINX_DOCKER_GEN_CONTAINER=nginx_gen" ]
#
#  labels = {
#    "container_group" = "proxy"
#  }
#
#  log_driver = "gelf"
#  log_opts = {
#    "gelf-address" = "udp://localhost:12201"
#    "labels" = "container_group"
##  }
#}

########################################################
# Web Containers
########################################################
#create bamboo container
resource "docker_container" "bamboo" {
  image = "${docker_image.bamboo.latest}"
  name = "bamboo"
  hostname = "bamboo"
  must_run="true"
  networks = [ "${docker_network.tooling.name}" ]

  ports {
    internal = 8085
    external = 8085
  }

  env = ["X_PROXY_NAME=bamboo.hung.ts0.org", "X_PROXY_PORT=80", "X_PROXY_SCHEME=http", "VIRTUAL_HOST=bamboo.hung.ts0.org", "LETSENCRYPT_HOST=bamboo.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us" ]

  labels = {
    "container_group" = "tooling"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

#create artifactory container
resource "docker_container" "artifactory" {
  image = "${docker_image.artifactory.latest}"
  name = "artifactory"
  hostname = "artifactory"
  must_run="true"
  networks = [ "${docker_network.tooling.name}" ]

  ports {
    internal = 8081
    external = 8081
  }

  env = [ "VIRTUAL_HOST=artifactory.hung.ts0.org", "LETSENCRYPT_HOST=artifactory.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true" ]

  labels = {
    "container_group" = "tooling"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

#create splunk container
resource "docker_container" "splunk" {
  image = "${docker_image.splunk.latest}"
  name = "splunk"
  hostname = "splunk"
  must_run="true"
  networks = [ "${docker_network.tooling.name}" ]

  ports {
    internal = 8000
    external = 8000
  }

  ports {
    internal = 9997
    external = 9997
  }

  env = ["SPLUNK_START_ARGS=--accept-license --answer-yes", "VIRTUAL_HOST=splunk.hung.ts0.org", "VIRTUAL_PORT=8000", "LETSENCRYPT_HOST=splunk.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true" ]

  labels = {
    "container_group" = "tooling"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

########################################################
# Puppet Containers
########################################################
#create puppet servers
resource "docker_container" "puppet" {
  image = "${docker_image.puppet.latest}"
  name = "puppet"
  hostname = "puppet"
  must_run="true"
  networks = [ "${docker_network.puppet.name}" ]

  dns_search = [ "puppet" ]

  ports {
    internal = 8140
    external = 8140
  }

  volumes {
    host_path  = "/puppet/code"
    container_path = "/etc/puppetlabs/code/"
    read_only = false
  }

  volumes {
    host_path  = "/puppet/ssl"
    container_path = "/etc/puppetlabs/puppet/ssl/"
    read_only = false
  }

  volumes {
    host_path  = "/puppet/serverdata"
    container_path = "/opt/puppetlabs/server/data/puppetserver/"
    read_only = false
  }

  env = ["PUPPETDB_SERVER_URLS=https://puppetdb.puppet:8081"]

  labels = {
    "container_group" = "puppet"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

#create puppetdb-postgres container
resource "docker_container" "puppetdb-postgres" {
  image = "${docker_image.puppetdb-postgres.latest}"
  name = "postgres"
  hostname = "postgres"
  must_run="true"
  networks = [ "${docker_network.puppet.name}" ]

  dns_search = [ "puppet" ]

  ports {
    internal = 5432
    external = 5432
  }

  volumes {
    host_path  = "/puppet/puppetdb-postgres/data"
    container_path = "/var/lib/postgresql/data/"
    read_only = false
  }

  env = ["POSTGRES_PASSWORD=puppetdb", "POSTGRES_USER=puppetdb"]

  labels = {
    "container_group" = "puppet"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

#create puppetdb container
resource "docker_container" "puppetdb" {
  image = "${docker_image.puppetdb.latest}"
  name = "puppetdb"
  hostname = "puppetdb"
  must_run="true"
  networks = [ "${docker_network.puppet.name}" ]

  dns_search = [ "puppet" ]

  ports {
    internal = 8080
  }

  ports {
    internal = 8081
  }

  volumes {
    host_path  = "/puppet/puppetdb/ssl"
    container_path = "/etc/puppetlabs/puppet/ssl/"
    read_only = false
  }

  labels = {
    "container_group" = "puppet"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

#create puppetboard container
resource "docker_container" "puppetboard" {
  image = "${docker_image.puppetboard.latest}"
  name = "puppetboard"
  hostname = "puppetboard"
  must_run="true"
  networks = [ "${docker_network.puppet.name}" ]

  dns_search = [ "puppet" ]

  ports {
    internal = 8000
    external = 8001
  }

  env = [ "VIRTUAL_HOST=puppetboard.hung.ts0.org", "LETSENCRYPT_HOST=puppetboard.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true" ]

  labels = {
    "container_group" = "puppet"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

#create puppet explorer container
resource "docker_container" "puppetexplorer" {
  image = "${docker_image.puppetexplorer.latest}"
  name = "puppetexplorer"
  hostname = "puppetexplorer"
  must_run="true"
  networks = [ "${docker_network.puppet.name}" ]

  dns_search = [ "puppet" ]

  ports {
    internal = 80
  }

  env = [ "VIRTUAL_HOST=puppet-explorer.hung.ts0.org", "LETSENCRYPT_HOST=puppet-explorer.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true" ]

  labels = {
    "container_group" = "puppet"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

########################################################
# MuleSoft Containers
########################################################
# create app_dynamics container
resource "docker_container" "appdynamics" {
  image    = "${docker_image.appdynamics.latest}"
  name     = "appdynamics"
  hostname = "appdynamics"
  must_run = "true"
  networks = [ "${docker_network.tooling.name}" ]

  ports {
    internal = 8090
    external = 8090
  }

  labels = {
    "container_group" = "mulesoft"
  }

  env = [ "VIRTUAL_HOST=appdynamics.hung.ts0.org", "LETSENCRYPT_HOST=appdynamics.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true" ]

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

#create mmc container
resource "docker_container" "mmc" {
  image = "${docker_image.mmc.latest}"
  name = "mmc"
  hostname = "mmc"
  must_run="true"
  networks = [ "${docker_network.tooling.name}" ]

  ports {
    internal = 7777
    external = 7777
  }

  ports {
    internal = 8585
    external = 8585
  }

  env = [ "VIRTUAL_HOST=mmc.hung.ts0.org", "LETSENCRYPT_HOST=mmc.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true", "VIRTUAL_PORT=8585" ]

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

########################################################
# Logging Containers
########################################################

# Runs on your node(s) and forwards all logs to Logstash.
resource "docker_container" "filebeat" {
  image = "${docker_image.filebeat.latest}"
  name = "filebeat"
  hostname = "filebeat"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  volumes {
    host_path  = "/storage/filebeat/filebeat.yml"
    container_path = "/filebeat.yml"
    read_only = true
  }

  volumes {
    host_path  = "/var/log"
    container_path = "/host-logs"
    read_only = true
  }

  labels = {
    "container_group" = "logging"
  }

  log_driver = "gelf"
  log_opts = { 
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}

# Aggregates logs and forwards them to Elasticsearch.
resource "docker_container" "logstash" {
  image = "${docker_image.logstash.latest}"
  name = "logstash"
  hostname = "logstash"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  ports {
    internal = 5044
    external = 5044
  }

  ports {
    internal = 12201
    external = 12201
    protocol = "UDP"
  }

  volumes {
    host_path  = "/storage/logstash/config"
    container_path = "/config"
    read_only = true
  }

  volumes {
    host_path  = "/storage/logstash/patterns"
    container_path = "/opt/logstash/extra_patterns"
    read_only = true
  }

  command = [ "logstash", "-f", "/config" ]

  labels = {
    "container_group" = "logging"
  }

  log_driver = "gelf"
  # Logs are collected by the docker daemon. 
  # The daemon itself is not part of the docker-network, 
  # so cannot resolve / directly connect to a container, 
  # unless the container is exposing/publishing a port. 
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}

# Storage and search backend. Gets all logs from Logstash and is the backend that Kibana runs on.
resource "docker_container" "elasticsearch" {
  image = "${docker_image.elasticsearch.latest}"
  name = "elasticsearch"
  hostname = "elasticsearch"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  ports {
    internal = 9200
    external = 9200
  }

  volumes {
    host_path  = "/storage/elasticsearch"
    container_path = "/usr/share/elasticsearch/data"
    read_only = false
  }

  env = [ "ES_JAVA_OPTS=-Xms512m -Xmx512m" ]

  labels = {
    "container_group" = "logging"
  }
  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}

# Takes care of piling piling up Elasticsearch indices/logs. Can do many other things as well.
# Set up a cron or Jenkins job that runs "docker-compose run --rm curator --config /config.yml /action-file.yml" every once in a while.
resource "docker_container" "curator" {
  image = "${docker_image.curator.latest}"
  name = "curator"
  hostname = "curator"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  volumes {
    host_path  = "/storage/curator/action-file.yml"
    container_path = "/action-file.yml"
    read_only = true
  }

  volumes {
    host_path  = "/storage/curator/config.yml"
    container_path = "/config.yml"
    read_only = true
  }

  labels = {
    "container_group" = "logging"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}

# Pretty frontend to explore and check out all your logs.
resource "docker_container" "kibana" {
  image = "${docker_image.kibana.latest}"
  name = "kibana"
  hostname = "kibana"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  ports {
    internal = 5601
    external = 5601
  }

  volumes {
    host_path  = "/storage/kibana/config/"
    container_path = "/opt/kibana/config/"
    read_only = true
  }

  # fixes memory leak (https://github.com/elastic/kibana/issues/5170)
  env = [ "NODE_OPTIONS=--max-old-space-size=200", "VIRTUAL_HOST=kibana.hung.ts0.org", "LETSENCRYPT_HOST=kibana.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true" ]

  labels = {
    "container_group" = "logging"
  }
  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}


########################################################
# Monitoring Containers
########################################################
# Runs on node and forwards node(host) metrics to Prometheus.
resource "docker_container" "node_exporter" {
  image = "${docker_image.node_exporter.latest}"
  name = "node-exporter"
  hostname = "node-exporter"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  ports {
    internal = 9100
    external = 9100
  }

  labels = {
    "container_group" = "monitoring"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}

# Runs on the node and forwards container metrics to Prometheus.
resource "docker_container" "cadvisor" {
  image = "${docker_image.cadvisor.latest}"
  name = "cadvisor"
  hostname = "cadvisor"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]
  depends_on = ["docker_container.prometheus"]

  ports {
    internal = 8080
    external = 8080
  }

  volumes {
    host_path  = "/"
    container_path = "/rootfs"
    read_only = true
  }

  volumes {
    host_path  = "/var/run"
    container_path = "/var/run"
    read_only = false
  }

  volumes {
    host_path  = "/sys"
    container_path = "/sys"
    read_only = true
  }

  volumes {
    host_path  = "/var/lib/docker/"
    container_path = "/var/lib/docker/"
    read_only = true
  }

  labels = {
    "container_group" = "monitoring"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}

# Storage and search backend. Gets all metrics from cAdvisor and Nodeexporter and is the backend that Grafana runs on.
resource "docker_container" "prometheus" {
  image = "${docker_image.prometheus.latest}"
  name = "prometheus"
  hostname = "prometheus"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  ports {
    internal = 9090
    external = 9090
  }

  volumes {
    host_path  = "/storage/prometheus/data"
    container_path = "/prometheus"
    read_only = false
  }

  volumes {
    host_path  = "/storage/prometheus/prometheus.yml"
    container_path = "/etc/prometheus/prometheus.yml"
    read_only = false
  }

  volumes {
    host_path  = "/storage/prometheus/rules"
    container_path = "/etc/prometheus"
    read_only = true
  }

  command = [ "-config.file=/etc/prometheus/prometheus.yml", "-storage.local.path=/prometheus", "-web.console.libraries=/etc/prometheus/console_libraries", "-web.console.templates=/etc/prometheus/consoles", "-web.listen-address=:9090", "-alertmanager.url=http://alertmanager:9093", "-storage.local.memory-chunks=300000", "-storage.local.retention=744h" ]

  labels = {
    "container_group" = "monitoring"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}

# Pretty frontend to explore and check out all your metrics.
resource "docker_container" "grafana" {
  image = "${docker_image.grafana.latest}"
  name = "grafana"
  hostname = "grafana"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  volumes {
    host_path  = "/storage/grafana"
    container_path = "/var/lib/grafana"
    read_only = false
  }

  env = [ "GF_SECURITY_ADMIN_USER=admin", "VIRTUAL_HOST=grafana.hung.ts0.org", "LETSENCRYPT_HOST=grafana.hung.ts0.org", "LETSENCRYPT_EMAIL=michael@dodwell.us", "LETSENCRYPT_TEST=true" ]

  ports {
    internal = 3000
    external = 3000
  }

  labels = {
    "container_group" = "monitoring"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }
}

resource "docker_container" "alertmanager" {
  image = "${docker_image.alertmanager.latest}"
  name = "alertmanager"
  hostname = "alertmanager"
  must_run="true"
  networks = [ "${docker_network.logging.name}" ]

  ports {
    internal = 9093
    external = 9093
  }

  volumes {
    host_path  = "/storage/alertmanager/config.yml"
    container_path = "/etc/alertmanager/config.yml"
    read_only = true
  }

  volumes {
    host_path  = "/storage/alertmanager/data"
    container_path = "/alertmanager"
    read_only = false
  }

  command = [ "-config.file=/etc/alertmanager/config.yml", "-storage.path=/alertmanager" ]

  labels = {
    "container_group" = "monitoring"
  }

  log_driver = "gelf"
  log_opts = {
    "gelf-address" = "udp://localhost:12201"
    "labels" = "container_group"
  }

}

