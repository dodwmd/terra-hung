
module "ami" {
  source = "github.com/terraform-community-modules/tf_aws_ubuntu_ami"
  region = "${var.region}"
  distribution = "${var.distro}"
  architecture = "amd64"
  virttype = "hvm"
  storagetype = "ebs-ssd"
}

module "cloudflare" {
  source = "./modules/cloudflare"
  domain = "ts0.org"
  subdomain = "hung.ts0.org"
  nameservers = "${join(",", aws_route53_zone.main.name_servers)}"
  email = "${var.cf_email}"
  token = "${var.cf_token}"
}

module "cloudflare-yellowline" {
  source = "./modules/cloudflare"
  domain = "yellowline.ninja"
  subdomain = "rancher.yellowline.ninja"
  nameservers = "${join(",", aws_route53_zone.yellowline.name_servers)}"
  email = "${var.cf_email}"
  token = "${var.cf_token}"
}


module "rancher" {

    source = "./modules/rancher"

    vpc_id                 = "${aws_vpc.default.id}"
    vpc_region             = "${var.region}"
    vpc_private_subnets    = "${aws_subnet.private_subnet.0.cidr_block},${aws_subnet.private_subnet.1.cidr_block},${aws_subnet.private_subnet.2.cidr_block}"
    vpc_private_subnet_ids = "${aws_subnet.private_subnet.0.id},${aws_subnet.private_subnet.1.id},${aws_subnet.private_subnet.2.id}"
    jump_sg                = "${aws_security_group.allow-ssh.id}"
    elb_sg                 = "${aws_security_group.allow-elb.id}"
    ami                    = "${module.ami.ami_id}"
    server_instance_type   = "m4.large"

    # Server config
    server_name      = "master"
    server_hostname  = "master.rancher.yellowline.ninja"
    key_name         = "michael.d.dodwell"
    server_subnet_id = "${aws_subnet.public_subnet.0.id}"
    server_version   = "v1.3.3"
    server_security_group_id = "${aws_security_group.default-terraform.id}"

    # SSL
    ssl_email = "michael@dodwell.us"

    # Database
    database_address  = "${aws_instance.mysql-rancher.private_ip}"
    database_username = "rancher"
    database_password = "rancher"
    database_name     = "rancher"

    # Name your cluster and provide the autoscaling group name and security group id.
    # See examples below.
    cluster_name                       = "poc"
    cluster_autoscaling_group_name     = "${aws_autoscaling_group.rancher_node.id}"
    cluster_instance_security_group_id = "${aws_security_group.allow-all-from-int.id}"

}

#module "glusterfs" {
#
#    source = "./modules/gluster"
#
#    vpc_id                 = "${aws_vpc.default.id}"
#    vpc_region             = "${var.region}"
#    vpc_private_subnets    = "${aws_subnet.private_subnet.0.cidr_block},${aws_subnet.private_subnet.1.cidr_block},${aws_subnet.private_subnet.2.cidr_block}"
#    vpc_private_subnet_ids = "${aws_subnet.private_subnet.0.id},${aws_subnet.private_subnet.1.id},${aws_subnet.private_subnet.2.id}"
#    jump_sg                = "${aws_security_group.allow-ssh.id}"
#    elb_sg                 = "${aws_security_group.allow-elb.id}"
#    ami                    = "${module.ami.ami_id}"
#
#    # Server config
#    key_name         = "michael.d.dodwell"
#    server_security_group_id = "${aws_security_group.default-terraform.id}"
#    server_subnet_id = "${aws_subnet.public_subnet.0.id}"
#
#
#
#}
