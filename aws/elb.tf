resource "aws_elb" "rancher_public_elb" {
  subnets                     = ["${aws_subnet.public_subnet.*.id}"]
  name                        = "dodwmd-rancher-public-elb"
  security_groups             = ["${aws_security_group.allow-elb.id}", "${aws_security_group.default-terraform.id}"]
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400


  # container:nginx
  listener {
    instance_port             = 80
    instance_protocol         = "tcp"
    lb_port                   = 80
    lb_protocol               = "tcp"
  }

  # container:nginx
  listener {
    instance_port             = 443
    instance_protocol         = "tcp"
    lb_port                   = 443
    lb_protocol               = "tcp"
  }

  # container:registry
  listener {
    instance_port             = 5000
    instance_protocol         = "tcp"
    lb_port                   = 5000
    lb_protocol               = "tcp"
  }

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "TCP:80"
    interval = 30
  }

  tags {
    Name                      = "dodwmd_rancher_public_elb"
    Owner                     = "dodwmd"
  }
}

resource "aws_elb" "docker_services_elb" {
  subnets                     = ["${aws_subnet.private_subnet.*.id}"]
  name                        = "docker-services-elb"
  security_groups             = ["${aws_security_group.allow-elb.id}", "${aws_security_group.default-terraform.id}"]
  idle_timeout                = 400
  connection_draining         = true
  internal                    = true
  connection_draining_timeout = 400


  # container:splunk
  listener {
    instance_port             = 9997
    instance_protocol         = "tcp"
    lb_port                   = 9997
    lb_protocol               = "tcp"
  }

  # container:splunk
  listener {
    instance_port             = 8089
    instance_protocol         = "tcp"
    lb_port                   = 8089
    lb_protocol               = "tcp"
  }

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    target = "TCP:9997"
    interval = 30
  }

  tags {
    Name                      = "dodwmd_public_elb"
    Owner                     = "dodwmd"
  }
}
