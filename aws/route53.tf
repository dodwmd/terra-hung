##
# Domains
##

resource "aws_route53_delegation_set" "main" {
    reference_name = "CloudFlare"
}

resource "aws_route53_zone" "main" {
  name = "hung.ts0.org"
  delegation_set_id = "${aws_route53_delegation_set.main.id}"
}

resource "aws_route53_delegation_set" "yellowline" {
    reference_name = "CF_Yellowline"
}

resource "aws_route53_zone" "yellowline" {
  name = "rancher.yellowline.ninja"
  delegation_set_id = "${aws_route53_delegation_set.yellowline.id}"
}

##
# Hosts
##

resource "aws_route53_record" "rancher-mysql" {
  zone_id = "${aws_route53_zone.yellowline.zone_id}"
  name    = "mysql"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.rancher-mysql.private_ip}"]

  lifecycle {
      create_before_destroy = true
  }
}

resource "aws_route53_record" "rancher-server" {
  zone_id = "${aws_route53_zone.yellowline.zone_id}"
  name    = "master"
  type    = "A"
  ttl     = "300"
  records = ["${module.rancher.server_public_ip}"]

  lifecycle {
      create_before_destroy = true
  }
}

resource "aws_route53_record" "jump" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "jump"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.jump.public_ip}"]

  lifecycle {
      create_before_destroy = true
  }
}

##
# DNS Aliases
##

resource "aws_route53_record" "kibana" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "kibana"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "puppet-explorer" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "puppet-explorer"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "puppetboard" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "puppetboard"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "jira" {
  zone_id = "${aws_route53_zone.yellowline.zone_id}"
  name    = "jira"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "bamboo" {
  zone_id = "${aws_route53_zone.yellowline.zone_id}"
  name    = "bamboo"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "grafana" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "grafana"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "appdynamics" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "appdynamics"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "mmc" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "mmc"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "registry" {
  zone_id = "${aws_route53_zone.yellowline.zone_id}"
  name    = "registry"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.registry.public_ip}"]

  lifecycle {
      create_before_destroy = true
  }
}

resource "aws_route53_record" "artifactory" {
  zone_id = "${aws_route53_zone.yellowline.zone_id}"
  name    = "artifactory"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "www"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "splunk-yellowline" {
  zone_id = "${aws_route53_zone.yellowline.zone_id}"
  name    = "splunk"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "splunk" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "splunk"
  type    = "A"
  alias   = {
    name = "${aws_elb.rancher_public_elb.dns_name}"
    zone_id = "${aws_elb.rancher_public_elb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "splunk-services" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name    = "splunk-services"
  type    = "A"
  alias   = {
    name = "${aws_elb.docker_services_elb.dns_name}"
    zone_id = "${aws_elb.docker_services_elb.zone_id}"
    evaluate_target_health = true
  }
}
