# Generate CA

resource "tls_private_key" "root" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P521"
}

resource "tls_self_signed_cert" "root" {
  key_algorithm   = "ECDSA"
  private_key_pem = "${tls_private_key.root.private_key_pem}"

  validity_period_hours = 26280
  early_renewal_hours   = 8760

  is_ca_certificate = true

  allowed_uses = ["cert_signing"]

  subject {
    common_name         = "Accenture Hung Root"
    organization        = "Hung Enterprises"
    organizational_unit = "Department of Certificate Authority"
    street_address      = ["879 POC Drive"]
    locality            = "Brisbane"
    province            = "QLD"
    country             = "AU"
    postal_code         = "4000"
  }
}

# Generate and sign certificate for rancher-mysql server
resource "tls_private_key" "rancher-mysql" {
  algorithm = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "rancher-mysql" {
  key_algorithm = "ECDSA"
  private_key_pem = "${tls_private_key.rancher-mysql.private_key_pem}"

  subject {
    common_name = "mysql.rancher.yellowline.ninja"
    organization = "Accenture"
  }
}

resource "tls_locally_signed_cert" "rancher-mysql" {
  cert_request_pem = "${tls_cert_request.rancher-mysql.cert_request_pem}"

  ca_key_algorithm   = "${tls_private_key.root.algorithm}"
  ca_private_key_pem = "${tls_private_key.root.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.root.cert_pem}"

  validity_period_hours = 17520
  early_renewal_hours   = 8760

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]

}

# Generate and sign certificate for docker server
resource "tls_private_key" "docker-client" {
  algorithm = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "docker-client" {
  key_algorithm = "ECDSA"
  private_key_pem = "${tls_private_key.docker-client.private_key_pem}"

  subject {
    common_name = "docker.hung.ts0.org"
    organization = "Accenture"
  }
}

resource "tls_locally_signed_cert" "docker-client" {
  cert_request_pem = "${tls_cert_request.docker-client.cert_request_pem}"

  ca_key_algorithm   = "${tls_private_key.root.algorithm}"
  ca_private_key_pem = "${tls_private_key.root.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.root.cert_pem}"

  validity_period_hours = 17520
  early_renewal_hours   = 8760

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "client_auth",
  ]
}
