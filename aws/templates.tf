#
# Jump host
#

data "template_file" "jump" {
  template               = "${file("${path.module}/files/cloudinit/default.template")}"

  vars {
    hostname             = "jump.hung.ts0.org"
  }
}

data "template_cloudinit_config" "jump" {
  gzip           = false
  base64_encode  = false

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.jump.rendered}"
  }

}

#
# rancher-mysql
#

data "template_file" "rancher-mysql" {
  template       = "${file("${path.module}/files/cloudinit/default.template")}"

  vars {
    hostname     = "rancher-mysql.hung.ts0.org"
  }
}

data "template_cloudinit_config" "rancher-mysql" {
  gzip           = true
  base64_encode  = true

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.rancher-mysql.rendered}"
  }
}

data "template_file" "docker-override-rancher-mysql" {
  template       = "${file("${path.module}/files/docker/override.conf")}"

  vars {
    hostname     = "eth0"
  }
}

##
# registry
##

# User-data template
data "template_file" "registry_cloudconfig" {
  template       = "${file("${path.module}/files/cloudinit/default.template")}"

  vars {
    hostname     = "registry.rancher.yellowline.ninja"
  }
}

# User-data script template
data "template_file" "registry_script" {

    template = "${file("${path.module}/files/cloudinit/registry-usersdata.template")}"
}

data "template_cloudinit_config" "registry_user_data" {
  gzip           = true
  base64_encode  = true

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.registry_cloudconfig.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.registry_script.rendered}"
  }
}

##
# mysql
##

# User-data template
data "template_file" "mysql_cloudconfig" {
  template       = "${file("${path.module}/files/cloudinit/default.template")}"

  vars {
    hostname     = "mysql.rancher.yellowline.ninja"
  }
}

# User-data script template
data "template_file" "mysql_script" {

    template = "${file("${path.module}/files/cloudinit/mysql-usersdata.template")}"
}

data "template_cloudinit_config" "mysql_user_data" {
  gzip           = true
  base64_encode  = true

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.mysql_cloudconfig.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.mysql_script.rendered}"
  }
}
