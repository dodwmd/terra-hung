# Autoscaling launch configuration
resource "aws_launch_configuration" "rancher_node" {

    #name = "rancher-node-launch-conf-2"

    # Ubuntu linux, ${var.region}
    image_id = "${module.ami.ami_id}"

    # No public ip when instances are placed in private subnets. See notes
    # about creating an ELB to proxy public traffic into the cluster.
    associate_public_ip_address = false

    # Security groups
    security_groups = [
        "${aws_security_group.default-terraform.id}",
        "${module.rancher.host_security_group_id}"
    ]

    # drive to store docker images
    ebs_block_device {
      device_name = "/dev/xvdb"
      volume_size = 50
      volume_type = "standard"
      delete_on_termination = true
    }

    # Key
    # NOTE: It's a good idea to use the same key as the Rancher server here.
    key_name = "michael.d.dodwell"

    # Add rendered userdata template
    user_data = "${module.rancher.host_user_data}"

    # Misc
    instance_type = "m4.large"
    enable_monitoring = true

    iam_instance_profile = "${module.rancher.rancher_node_instance_profile}"
    
    lifecycle {
        create_before_destroy = true
    }

}

# Autoscaling group
resource "aws_autoscaling_group" "rancher_node" {

    #name = "rancher-host-ASG"
    launch_configuration = "${aws_launch_configuration.rancher_node.name}"
    min_size = "3"
    max_size = "10"
    desired_capacity = "3"
    health_check_grace_period = 180
    health_check_type = "EC2"
    force_delete = false
    termination_policies = ["OldestInstance"]

    # Add ELB's here if you're proxying public traffic into the cluster
    load_balancers = ["${aws_elb.rancher_public_elb.name}", "${aws_elb.docker_services_elb.name}" ]

    # Target subnets
    vpc_zone_identifier = ["${aws_subnet.private_subnet.0.id}", "${aws_subnet.private_subnet.1.id}", "${aws_subnet.private_subnet.2.id}"]

    tag {
      key = "Owner"
      value = "dodwmd"
      propagate_at_launch = true
    }

    tag {
      key = "Name"
      value = "rancher-host-asg"
      propagate_at_launch = true
    }

    lifecycle {
        create_before_destroy = true
    }

    depends_on = ["aws_instance.rancher-mysql"]

}
