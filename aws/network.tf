#
# VPC
#
resource "aws_vpc" "default" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags {
    Name               = "dodwmd_docker_vpc"
    Owner              = "dodwmd"
  }
}

#
# DHCP
#
resource "aws_vpc_dhcp_options" "default" {
  domain_name = "hung.ts0.org"
  domain_name_servers = ["AmazonProvidedDNS"]
  
  tags {
    Name      = "dodwmd_docker_vpc"
    Owner     = "dodwmd"
  }
}

resource "aws_vpc_dhcp_options_association" "default" {
  vpc_id          = "${aws_vpc.default.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.default.id}"
}

#
# EIPs
#
resource "aws_eip" "natgw" {
  vpc   = true
  count = "${length( split( ",", lookup( var.azs, var.region ) ) )}"
}

#
# SUBNETS
#

# PUBLIC SUBNETS
resource "aws_subnet" "public_subnet" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${cidrsubnet( var.vpc_cidr, var.subnet_bits, count.index )}"
  availability_zone = "${var.region}${element( split( ",", lookup( var.azs, var.region ) ), count.index )}"
  count             = "${length( split( ",", lookup( var.azs, var.region ) ) )}"
  tags {
    Name            = "dodwmd_docker_public_subnet_${element( split( ",", lookup( var.azs, var.region ) ), count.index )}"
    Owner           = "dodwmd"
  }
}

# PRIVATE SUBNETS
resource "aws_subnet" "private_subnet" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "${cidrsubnet( var.vpc_cidr, var.subnet_bits, ( length( split( ",", lookup( var.azs, var.region ) ) ) + count.index ) )}"
  availability_zone = "${var.region}${element( split( ",", lookup( var.azs, var.region ) ), count.index )}"
  count             = "${length( split( ",", lookup( var.azs, var.region ) ) )}"
  tags {
    Name            = "dodwmd_docker_private_subnet_${element( split( ",", lookup( var.azs, var.region ) ), count.index )}"
    Owner           = "dodwmd"
  }
}

#
# GATEWAYS
#

# INTERNET GW
resource "aws_internet_gateway" "default_igw" {
  vpc_id    = "${aws_vpc.default.id}"
  tags {
    Name    = "dodwmd_docker_igw"
    Owner   = "dodwmd"
  }
}

# NAT GW
resource "aws_nat_gateway" "default_natgw" {
  allocation_id = "${element( aws_eip.natgw.*.id, count.index )}"
  subnet_id     = "${element( aws_subnet.public_subnet.*.id, count.index )}"
  count         = "${length( split( ",", lookup( var.azs, var.region ) ) )}"
  depends_on    = ["aws_internet_gateway.default_igw"]
  # Tags not supported
}

#
# ROUTES
#

# DEFAULT ROUTE TO IGW
resource "aws_route_table" "default_to_igw" {
  vpc_id       = "${aws_vpc.default.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.default_igw.id}"
  }
  tags {
    Name       = "dodwmd_docker_default_to_igw"
    Owner      = "dodwmd"
  }
}

resource "aws_route_table" "default_to_natgw" {
  vpc_id           = "${aws_vpc.default.id}"
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${element( aws_nat_gateway.default_natgw.*.id, count.index )}"
  }
  count            = "${length( split( ",", lookup( var.azs, var.region ) ) )}"
  tags {
    Name           = "dodwmd_docker_default_to_natgw_${element( split( ",", lookup( var.azs, var.region ) ), count.index )}"
    Owner          = "dodwmd"
  }
}

resource "aws_route_table_association" "public_subnet_and_default_to_igw" {
  subnet_id      = "${element( aws_subnet.public_subnet.*.id, count.index )}"
  route_table_id = "${aws_route_table.default_to_igw.id}"
  count          = "${length( split( ",", lookup( var.azs, var.region ) ) )}"
  # Tags not supported
}

resource "aws_route_table_association" "private_subnet_and_default_to_natgw" {
  subnet_id      = "${element( aws_subnet.private_subnet.*.id, count.index )}"
  route_table_id = "${element( aws_route_table.default_to_natgw.*.id, count.index )}"
  count          = "${length( split( ",", lookup( var.azs, var.region ) ) )}"
  # Tags not supported
}
