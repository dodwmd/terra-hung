#!/bin/bash

# Setup partition table
/bin/sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | /sbin/fdisk /dev/xvdb
  o # clear the in memory partition table
  n # new partition
  p # primary partition
  1 # partition number 1
    # default - start at beginning of disk 
    # entire size of parttion
  p # print the in-memory partition table
  w # write the partition table
EOF

# Make filesystem
/sbin/mkfs.ext4 -m0 /dev/xvdb1

# Make mount point
/bin/mkdir /var/lib/docker

# Mount drive and add to fstab
/bin/mount /dev/xvdb1 /var/lib/docker && /bin/grep xvdb1 /etc/mtab | /usr/bin/tee -a /etc/fstab
