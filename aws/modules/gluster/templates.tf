
# User-data template
data "template_file" "gluster_server_user_data" {

    template = "${file("${path.module}/files/gluster-userdata.template")}"

    vars {

    }

}

