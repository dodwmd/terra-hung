variable "key_name" {
}

variable "ami" {
}
variable "server_subnet_id" {
}
variable "server_security_group_id" {
}
variable "server_instance_type" {
  default = "t2.micro"
}
variable "vpc_id" {
}
variable "vpc_region" {
}
variable "vpc_private_subnets" {
}
variable "vpc_private_subnet_ids" {
}

variable "jump_sg" {
}

variable "elb_sg" {
}
