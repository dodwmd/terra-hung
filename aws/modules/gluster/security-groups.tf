# Rancher server security group
resource "aws_security_group" "gluster_server_sg" {

    name = "gluster_server_sg"
    description = "Rules for the Rancher server instance."
    vpc_id = "${var.vpc_id}"

    tags {
        Name = "gluster_server_sg"
        ManagedBy = "terraform"
    }

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming all - FIXME - Should keep this locked down.. enable for testing purposes only
resource "aws_security_group_rule" "gluster_server_all_ingress" {

    security_group_id = "${aws_security_group.gluster_server_sg.id}"
    type = "ingress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }
}

# Outgoing all - FIXME - Should keep this locked down.. enable for testing purposes only
resource "aws_security_group_rule" "gluster_server_all_egress" {

    security_group_id = "${aws_security_group.gluster_server_sg.id}"
    type = "egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }
}

#output "server_security_group_id" {
#    value = "${aws_security_group.gluster_server_sg.id}"
#}
