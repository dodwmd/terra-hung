#############################
## GlusterFS Instances     ##
#############################

# Create instance
resource "aws_instance" "glusterfs" {

    # Ubuntu AMI
    ami = "${var.ami}"
    count = "3"

    # Target subnet - should be public
    subnet_id = "${var.server_subnet_id}"
    associate_public_ip_address = false

    # Security groups
    vpc_security_group_ids = [
        "${aws_security_group.gluster_server_sg.id}",
        "${var.server_security_group_id}"
    ]

    # SSH key
    key_name = "${var.key_name}"
    private_ip = "172.16.8.97"

    # User-data
    # Installs docker, starts containers and performs initial server setup
    #user_data = "${data.template_file.rancher_server_user_data.rendered}"

    # Instance profile - sets required permissions to access other aws resources
    #iam_instance_profile = "${aws_iam_instance_profile.rancher_server_instance_profile.id}"

    # Misc
    instance_type = "${var.server_instance_type}"

    # Ensure S3 bucket is created first
    #depends_on = [
    #    "aws_s3_bucket.server_credentials_bucket",
    #    "aws_iam_policy_attachment.rancher_node_s3_policy"
    #]

    tags {
        Name = "glusterfs"
        ManagedBy = "terraform"
    }

    lifecycle {
        create_before_destroy = true
    }

}

output "gluster_public_ip" {
    value = "${aws_instance.glusterfs.public_ip}"
}
