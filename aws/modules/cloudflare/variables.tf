variable "nameservers" {
  description = "nameservers for subdomain"
}

variable "email" {
  type = "string"
  default = ""
  description = "cloudflare login email"
}

variable "token" {
  type = "string"
  default = ""
  description = "auth token for cloudflare"
}

variable "domain" {
  description = "nameservers for subdomain"
}
variable "subdomain" {
  description = "nameservers for subdomain"
}

