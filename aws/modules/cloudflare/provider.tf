# Configure the CloudFlare provider
provider "cloudflare" {
    email = "${var.email}"
    token = "${var.token}"
}
