resource "cloudflare_record" "ns" {
    domain = "${var.domain}"
    name = "${var.subdomain}"
    type = "NS"
    ttl = "1"
    count = 4
    value = "${element(split(",", var.nameservers), count.index)}"
}
