#############################
## Rancher server instance ##
#############################

# User-data template
data "template_file" "rancher_server_cloudconfig" {
  template       = "${file("${path.module}/files/default.yaml")}"

  vars {
    hostname     = "${var.server_hostname}"
  }
}

# User-data script template
data "template_file" "rancher_server_script" {

    template = "${file("${path.module}/files/server-userdata.template")}"

    vars {

        # VPC config
        vpc_region = "${var.vpc_region}"

        # Server config
        server_version            = "${var.server_version}"
        server_credentials_bucket = "${aws_s3_bucket.server_credentials_bucket.id}"
        server_hostname           = "${var.server_hostname}"

        # SSL certificate
        ssl_email = "${var.ssl_email}"

        # SQS url
        sqs_url = "${aws_sqs_queue.autoscaling_hooks_queue.id}"

        # Database
        database_address  = "${var.database_address}"
        database_port     = "${var.database_port}"
        database_name     = "${var.database_name}"
        database_username = "${var.database_username}"
        database_password = "${var.database_password}"
        admin_username    = "${var.admin_username}"
        admin_password    = "${var.admin_password}"

    }

}

data "template_cloudinit_config" "rancher_server_user_data" {
  gzip           = true
  base64_encode  = true

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.rancher_server_cloudconfig.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.rancher_server_script.rendered}"
  }
}


# Create instance
resource "aws_instance" "rancher_server" {

    # Amazon linux
    ami = "${var.ami}"

    # Target subnet - should be public
    subnet_id = "${var.server_subnet_id}"
    associate_public_ip_address = true

    # Security groups
    vpc_security_group_ids = [
        "${aws_security_group.rancher_server_sg.id}",
        "${var.server_security_group_id}"
    ]

    # SSH key
    key_name = "${var.key_name}"

    # User-data
    # Installs docker, starts containers and performs initial server setup
    user_data = "${data.template_cloudinit_config.rancher_server_user_data.rendered}"

    # Instance profile - sets required permissions to access other aws resources
    iam_instance_profile = "${aws_iam_instance_profile.rancher_server_instance_profile.id}"

    # Misc
    instance_type = "${var.server_instance_type}"

    # Ensure S3 bucket is created first
    depends_on = [
        "aws_s3_bucket.server_credentials_bucket",
        "aws_iam_policy_attachment.rancher_node_s3_policy"
    ]

    tags {
        Name = "${var.server_name}"
        ManagedBy = "terraform"
    }

    lifecycle {
        create_before_destroy = true
    }

}

output "server_public_ip" {
    value = "${aws_instance.rancher_server.public_ip}"
}

output "server_private_ip" {
    value = "${aws_instance.rancher_server.private_ip}"
}

