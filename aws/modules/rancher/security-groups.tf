# Rancher server security group
resource "aws_security_group" "rancher_host_sg" {

    name = "rancher_host_sg"
    description = "Rules for the Rancher host instances."
    vpc_id = "${var.vpc_id}"

    tags {
        Name = "rancher_host_sg"
        Owner = "dodwmd"
        ManagedBy = "terraform"
    }

    lifecycle {
        create_before_destroy = true
    }

}

# Attach IPSEC rules to host instance security group.
# Enables the rancher overlay network for connected hosts.
# Traffic only allowed to and from other machines with this security group.
resource "aws_security_group_rule" "ipsec_ingress_1" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "ingress"
    from_port = 4500
    to_port = 4500
    protocol = "udp"
    source_security_group_id = "${aws_security_group.rancher_host_sg.id}"

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_security_group_rule" "ipsec_egress_1" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "egress"
    from_port = 4500
    to_port = 4500
    protocol = "udp"
    source_security_group_id = "${aws_security_group.rancher_host_sg.id}"

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_security_group_rule" "ipsec_ingress_2" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "ingress"
    from_port = 500
    to_port = 500
    protocol = "udp"
    source_security_group_id = "${aws_security_group.rancher_host_sg.id}"

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_security_group_rule" "ipsec_egress_2" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "egress"
    from_port = 500
    to_port = 500
    protocol = "udp"
    source_security_group_id = "${aws_security_group.rancher_host_sg.id}"

    lifecycle {
        create_before_destroy = true
    }

}


# Outgoing HTTP
# Allows pulling of remote docker images, installing packages, etc.
resource "aws_security_group_rule" "http_egress" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "egress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing HTTPS
# Allows pulling of remote docker images, installing packages, etc.
resource "aws_security_group_rule" "https_egress" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "egress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming SSH from jump server
resource "aws_security_group_rule" "rancher_node_ssh_ingress" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    source_security_group_id = "${var.jump_sg}"

    lifecycle {
        create_before_destroy = true
    }

}

# Rancher server security group
resource "aws_security_group" "rancher_server_sg" {

    name = "rancher_server_sg"
    description = "Rules for the Rancher server instance."
    vpc_id = "${var.vpc_id}"

    tags {
        Name = "${var.server_name}"
        ManagedBy = "terraform"
    }

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing HTTP to anywhere
resource "aws_security_group_rule" "rancher_server_http_egress" {

    security_group_id = "${aws_security_group.rancher_server_sg.id}"
    type = "egress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing HTTPS to anywhere
resource "aws_security_group_rule" "rancher_server_https_egress" {

    security_group_id = "${aws_security_group.rancher_server_sg.id}"
    type = "egress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming SSH from jump server
resource "aws_security_group_rule" "rancher_server_ssh_ingress" {

    security_group_id = "${aws_security_group.rancher_server_sg.id}"
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    source_security_group_id = "${var.jump_sg}"

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming HTTP from anywhere
# Nginx will redirect all http traffic to https
resource "aws_security_group_rule" "rancher_server_http_ingress" {

    security_group_id = "${aws_security_group.rancher_server_sg.id}"
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming HTTPS from anywhere
resource "aws_security_group_rule" "rancher_server_https_ingress" {

    security_group_id = "${aws_security_group.rancher_server_sg.id}"
    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing SSH to anywhere
# Allows configuration of rancher host instances via the UI
resource "aws_security_group_rule" "rancher_server_ssh_egress" {

    security_group_id = "${aws_security_group.rancher_server_sg.id}"
    type = "egress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing docker API to anywhere
# Allows configuration of rancher host instances via the UI
resource "aws_security_group_rule" "rancher_server_docker_egress" {

    security_group_id = "${aws_security_group.rancher_server_sg.id}"
    type = "egress"
    from_port = 2376
    to_port = 2376
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing mysql
resource "aws_security_group_rule" "rancher_server_mysql_egress" {

    security_group_id = "${aws_security_group.rancher_server_sg.id}"
    type = "egress"
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing all - FIXME - Should keep this locked down.. enable for testing purposes only
resource "aws_security_group_rule" "rancher_host_all_egress" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }
}

# Allow ELB to connect to any TCP rancher host port
resource "aws_security_group_rule" "rancher_host_elb_ingress" {

    security_group_id = "${aws_security_group.rancher_host_sg.id}"
    type = "ingress"
    from_port = 0
    to_port = 65535
    protocol = "tcp"
    source_security_group_id = "${var.elb_sg}"

    lifecycle {
        create_before_destroy = true
    }
}



output "server_security_group_id" {
    value = "${aws_security_group.rancher_server_sg.id}"
}

output "host_security_group_id" {
    value = "${aws_security_group.rancher_host_sg.id}"
}
