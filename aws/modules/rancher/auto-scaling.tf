# User-data template
# Registers the instance with the rancher server environment

# User-data template
data "template_file" "rancher_node_cloudconfig" {
  template       = "${file("${path.module}/files/node-default.yaml")}"

}

# User-data script template
data "template_file" "rancher_node_script" {

    template = "${file("${path.module}/files/hosts-userdata.template")}"
    vars {
        # VPC config
        vpc_region                = "${var.vpc_region}"

        # Server config
        server_credentials_bucket = "${aws_s3_bucket.server_credentials_bucket.id}"

        cluster_name              = "${var.cluster_name}"
        cluster_instance_labels   = "${var.cluster_instance_labels}"
        server_hostname           = "${var.server_hostname}"
        docker_daemon_options     = "${var.docker_daemon_options}"
    }

}

data "template_cloudinit_config" "rancher_node_user_data" {
  gzip           = false
  base64_encode  = false

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.rancher_node_cloudconfig.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.rancher_node_script.rendered}"
  }
}

# Lifecycle hook
# Triggered when an instance should be removed from the autoscaling
# group. Publishes a message to the supplied SQS queue so that the host
# can be removed from the Rancher server before shutting down.
resource "aws_autoscaling_lifecycle_hook" "cluster_instance_terminating_hook" {

    name = "cluster_instance_terminating_hook"
    autoscaling_group_name = "${var.cluster_autoscaling_group_name}"
    lifecycle_transition = "autoscaling:EC2_INSTANCE_TERMINATING"
    default_result = "CONTINUE"

    # 10 mins for rancher server to remove instance
    heartbeat_timeout = 600

    # Notification SQS queue
    notification_target_arn = "${aws_sqs_queue.autoscaling_hooks_queue.arn}"

    role_arn = "${aws_iam_role.lifecycle_role.arn}"

    lifecycle {
        create_before_destroy = true
    }

}

output "host_user_data" {
    value = "${data.template_cloudinit_config.rancher_node_user_data.rendered}"
}
