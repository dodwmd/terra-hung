# Autoscaling lifecycle hook role
# Allows lifecycle hooks to add messages to the SQS queue
resource "aws_iam_role" "lifecycle_role" {

    name = "${var.cluster_name}-lifecycle-hooks"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "autoscaling.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

    lifecycle {
        create_before_destroy = true
    }

}

# Attach policy document for access to the sqs queue
resource "aws_iam_role_policy" "lifecycle_role_policy" {
    name = "${var.cluster_name}-lifecycle-hooks-policy"
    role = "${aws_iam_role.lifecycle_role.id}"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Resource": "${aws_sqs_queue.autoscaling_hooks_queue.arn}",
    "Action": [
      "sqs:SendMessage",
      "sqs:GetQueueUrl",
      "sns:Publish"
    ]
  }]
}
EOF

    lifecycle {
        create_before_destroy = true
    }
    
}

# Rancher node instance profile
resource "aws_iam_instance_profile" "rancher_node_instance_profile" {

    name = "${var.cluster_name}-instance-profile"
    roles = [
        "${aws_iam_role.rancher_node_role.name}"
    ]

    lifecycle {
        create_before_destroy = true
    }

}

# Rancher server role
resource "aws_iam_role" "rancher_node_role" {

    name = "${var.cluster_name}-role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

    lifecycle {
        create_before_destroy = true
    }

}

# S3 bucket access
resource "aws_iam_policy_attachment" "rancher_node_s3_policy" {

    name = "rancher_node_s3_policy"
    policy_arn = "${aws_iam_policy.s3_server_credentials.arn}"
    roles = [
        "${aws_iam_role.rancher_server_role.name}",
        "${aws_iam_role.rancher_node_role.name}"
    ]

}

output "rancher_node_instance_profile" {
	value = "${aws_iam_instance_profile.rancher_node_instance_profile.id}"
}
##########################################
## Rancher server instance profile/role ##
##########################################

# Rancher server instance profile
resource "aws_iam_instance_profile" "rancher_server_instance_profile" {

    name = "${var.server_name}-instance-profile"
    roles = [
        "${aws_iam_role.rancher_server_role.name}"
    ]

    lifecycle {
        create_before_destroy = true
    }

}

# Rancher server role
resource "aws_iam_role" "rancher_server_role" {

    name = "${var.server_name}-role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

    lifecycle {
        create_before_destroy = true
    }

}

#####################
## Custom policies ##
#####################

# S3 credentials bucket access
# Allows the server to read/write api keys to the S3 bucket.
resource "aws_iam_policy" "s3_server_credentials" {

    name = "S3-credentials-access"
    path = "/"
    description = "Allow the Rancher server to access the credentials file in the S3 bucket."
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject",
        "s3:PutObject"
      ],
      "Resource": [
        "arn:aws:s3:::${aws_s3_bucket.server_credentials_bucket.id}/keys.txt"
      ]
    }
  ]
}
EOF

}

# SQS queue create & delete messages
# Allows the autoscaling hook app to receive and process messages
# published to the SQS queue.
resource "aws_iam_policy" "sqs_queue_access" {

    name = "SQS-queue-access"
    path = "/"
    description = "Allow the lifecycle hook app to receive and process SQS queue messages."
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sqs:DeleteMessage",
        "sqs:ReceiveMessage"
      ],
      "Resource": [
        "${aws_sqs_queue.autoscaling_hooks_queue.arn}"
      ]
    }
  ]
}
EOF

}

# Autoscaling lifecycle complete action
resource "aws_iam_policy" "autoscaling_complete_lifecycle_action" {

    name = "Allow-autoscaling-complete-action"
    path = "/"
    description = "Allow the lifecycle hook app send a complete action request that releases the instance for termination."
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:CompleteLifecycleAction"
      ],
      "Resource": "*"
    }
  ]
}
EOF

}

#############################
## Attach policies to role ##
#############################

# SQS access
resource "aws_iam_policy_attachment" "rancher_server_sqs_policy" {

    name = "rancher_server_sqs_policy"
    policy_arn = "${aws_iam_policy.sqs_queue_access.arn}"
    roles = [
        "${aws_iam_role.rancher_server_role.name}"
    ]
    
}

# Complete autoscaling hook
resource "aws_iam_policy_attachment" "complete_autoscaling_hooks" {

    name = "complete_autoscaling_hooks"
    policy_arn = "${aws_iam_policy.autoscaling_complete_lifecycle_action.arn}"
    roles = [
        "${aws_iam_role.rancher_server_role.name}"
    ]
    
}
