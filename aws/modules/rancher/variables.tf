#
# Rancher server details
#

variable "server_hostname" {
    description = "Hostname of the Rancher server."
}

# Cluster setup
variable "cluster_name" {
    description = "The name of the cluster. Best not to include non-alphanumeric characters. Will be used to name resources and tag instances."
}

variable "cluster_autoscaling_group_name" {
    description = "Name of the target autoscaling group."
}

variable "cluster_instance_security_group_id" {
    description = "ID of the security group used for host instances. Will be modified to include rancher specific rules."
}

variable "cluster_instance_labels" {
    description = "Additional labels to attach to host instances. Should be in the format: key=value&key2=value2"
    default = ""
}

# Docker options
variable "docker_daemon_options" {
    description = "Docker daemon options to write to the docker config file before startup."
    default = ""
}

#
# Rancher hosts variables
#

# Target VPC config
variable "vpc_id" {
    description = "ID of target VPC."
}
variable "vpc_region" {
    description = "Region of the target VPC"
}
variable "vpc_private_subnets" {
    description = "Comma separated list of private subnet ip address ranges."
}
variable "vpc_private_subnet_ids" {
    description = "Comma separated list of private subnet ids."
}
variable "server_security_group_id" {
    description = "Security group id of the Rancher server so we can restrict incoming traffic."
}

# Server config
variable "server_name" {
    description = "Name of the Rancher server. Best not to include non-alphanumeric characters."
}
variable "key_name" {
    description = "Key name for the Rancher server instance."
}
variable "server_subnet_id" {
    description = "Public subnet id in which to place the rancher server instance."
}
variable "server_version" {
    description = "Rancher server version ton install."
}
variable "server_instance_type" {
    description = "EC2 instance type to use for the rancher server."
    default = "t2.micro"
}
variable "ami" {
    description = "Ubuntu AMI id for the target region."
}

# SSL
variable "ssl_email" {
	description = "E-Mail address to use for Lets Encrypt account."
}

# Database
variable "database_address" {
    description = "Database server address."
}
variable "database_port" {
    description = "Database port."
    default = "3306"
}
variable "database_name" {
    description = "Database name."
    default = "rancherserverdb"
}
variable "database_username" {
    description = "Database username."
    default = "root"
}
variable "database_password" {
    description = "Database password."
}

# UI l/p
variable "admin_username" {
    description = "Rancher UI Administrator Username."
    default = "admin"
}
variable "admin_password" {
    description = "Rancher UI Administrator Password."
    default = "admin"
}

# SG
variable "jump_sg" {
    description = "Jump host SG"
}
variable "elb_sg" {
    description = "ELB SG"
}
