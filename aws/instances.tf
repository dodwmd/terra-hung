#
# Jump host
#

resource "aws_instance" "jump" {
  ami                         = "${module.ami.ami_id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.public_subnet.0.id}"
  key_name                    = "michael.d.dodwell"
  vpc_security_group_ids      = ["${aws_security_group.allow-ssh.id}", "${aws_security_group.default-terraform.id}"]
  associate_public_ip_address = true
  user_data                   = "${data.template_cloudinit_config.jump.rendered}"
  tags {
    Name                      = "jump"
    Owner                     = "dodwmd"
  }
}

##
# Registry Server
##

resource "aws_instance" "registry" {
  ami                         = "${module.ami.ami_id}"
  instance_type               = "t2.micro"
  subnet_id                   = "${aws_subnet.public_subnet.0.id}"
  key_name                    = "michael.d.dodwell"
  vpc_security_group_ids      = ["${aws_security_group.registry_sg.id}", "${aws_security_group.default-terraform.id}"]
  associate_public_ip_address = true
  user_data                   = "${data.template_cloudinit_config.registry_user_data.rendered}"

  # drive to store docker images
  ebs_block_device {
    device_name = "/dev/xvdb"
    volume_size = 100
    volume_type = "standard"
    delete_on_termination = true
  }

  tags {
    Name                      = "registry"
    Owner                     = "dodwmd"
  }
}

###########################################################################################
###########################################################################################
#
# Rancher
#
###########################################################################################
###########################################################################################
# Mysql Server
###########################################################################################

resource "aws_instance" "mysql-rancher" {
  ami                         = "${module.ami.ami_id}"
  instance_type               = "m4.large"
  subnet_id                   = "${aws_subnet.private_subnet.0.id}"
  key_name                    = "michael.d.dodwell"
  vpc_security_group_ids      = ["${aws_security_group.mysql_sg.id}", "${aws_security_group.default-terraform.id}"]
  associate_public_ip_address = false
  user_data                   = "${data.template_cloudinit_config.mysql_user_data.rendered}"

  tags {
    Name                      = "mysql"
    Owner                     = "dodwmd"
  }
}

