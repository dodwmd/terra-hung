output "elb" {
  value = "${aws_elb.rancher_public_elb.dns_name}"
}

output "jump" {
  value = "${aws_instance.jump.public_ip}"
}

output "master" {
  value = "${module.rancher.server_private_ip}"
}

output "nameservers" {
  value = "${aws_route53_zone.main.name_servers}"
}

#output "windows-docker" {
#  value = "${aws_instance.windows-docker.private_ip}"
#}
