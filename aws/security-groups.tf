resource "aws_security_group" "internal_splunk" {
  name        = "internal_splunk_sg"
  description = "Splunk Service Ports"
  vpc_id      = "${aws_vpc.default.id}"

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "udp"
    self      = true
  }
}

resource "aws_security_group" "allow-ssh" {
  name = "allow-ssh"
  description = "Allow all inbound SSH traffic"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
      to_port = 22
      from_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "allow-elb" {
  name = "allow-elb"
  description = "Allow ingress connections to the ELB"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
      to_port = 2376
      from_port = 2376
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      to_port = 5000
      from_port = 5000
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      to_port = 9997
      from_port = 9997
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      to_port = 8089
      from_port = 8089
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      to_port = 80
      from_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      to_port = 443
      from_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "allow-all-from-int" {
  name = "allow-all-from-int"
  description = "Allow all inbound traffic from internal security groups"
  vpc_id = "${aws_vpc.default.id}"

  ingress {
      to_port = 0
      from_port = 0
      protocol = "-1"
      security_groups = [ "${aws_security_group.default-terraform.id}" ]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

}

# Registry server security group
resource "aws_security_group" "registry_sg" {

    name = "registry_sg"
    description = "Rules for the registry server instance."
    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "registry.rancher.yellowline.ninja"
        ManagedBy = "terraform"
    }

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming HTTPS from anywhere
resource "aws_security_group_rule" "registry_https_ingress" {

    security_group_id = "${aws_security_group.registry_sg.id}"
    type = "ingress"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing to anywhere
resource "aws_security_group_rule" "registry_all_egress" {

    security_group_id = "${aws_security_group.registry_sg.id}"
    type = "egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming SSH from other servers
resource "aws_security_group_rule" "registry_ssh_ingress" {

    security_group_id = "${aws_security_group.registry_sg.id}"
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    source_security_group_id = "${aws_security_group.default-terraform.id}"

    lifecycle {
        create_before_destroy = true
    }

}

# Rancher mysql server security group
resource "aws_security_group" "mysql_sg" {

    name = "mysql_sg"
    description = "Rules for the mysql server instance."
    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "mysql.rancher.yellowline.ninja"
        ManagedBy = "terraform"
    }

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming MySQL from other servers
resource "aws_security_group_rule" "mysql_mysql_ingress" {

    security_group_id = "${aws_security_group.mysql_sg.id}"
    type = "ingress"
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    source_security_group_id = "${aws_security_group.default-terraform.id}"

    lifecycle {
        create_before_destroy = true
    }

}

# Incoming SSH from other servers
resource "aws_security_group_rule" "mysql_ssh_ingress" {

    security_group_id = "${aws_security_group.mysql_sg.id}"
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    source_security_group_id = "${aws_security_group.default-terraform.id}"

    lifecycle {
        create_before_destroy = true
    }

}

# Outgoing to anywhere
resource "aws_security_group_rule" "mysql_all_egress" {

    security_group_id = "${aws_security_group.mysql_sg.id}"
    type = "egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_security_group" "default-terraform" {
  name = "default-terraform"
  description = "Default SG to add to all instances"
  vpc_id = "${aws_vpc.default.id}"

}

