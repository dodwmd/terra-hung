variable "aws_access_key" {
  description = "The AWS access key."
}

variable "aws_secret_key" {
  description = "The AWS secret key."
}


#
# Networking
#
variable "vpc_cidr"          { default = "172.16.8.0/24" }
variable "subnet_bits"       { default = "3" }

variable "region"            { default = "ap-southeast-2" }
variable "distro"            { default = "xenial" }

variable "azs" {
  default = {
    "ap-southeast-2"    = "a,b,c"
  }
}

variable "cf_email" {
  type = "string"
  description = "cloudflare login email"
}

variable "cf_token" {
  type = "string"
  description = "auth token for cloudflare"
}

# Windows stuff
variable "admin_password" {
  description = "Windows Administrator password to login as."
}
